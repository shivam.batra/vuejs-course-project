const getRandomValue = function (min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
};

const app = Vue.createApp({
  data: function () {
    return {
      playerHealth: 100,
      monsterHealth: 100,
      currentRound: 0,
      winner: null,
      logMessages: [],
    };
  },

  methods: {
    startNewGame: function () {
      this.playerHealth = 100;
      this.monsterHealth = 100;
      this.winner = null;
      this.currentRound = 0;
      this.logMessages = [];
    },
    attackMonster: function () {
      this.currentRound++;
      const attackValue = getRandomValue(12, 5);
      this.monsterHealth -= attackValue;
      this.addLogMessage("player", "attack", `-${attackValue}`);
      if (this.monsterHealth < 0) this.monsterHealth = 0;

      this.attackPlayer();
    },
    attackPlayer: function () {
      const attackValue = getRandomValue(8, 12);
      this.playerHealth -= attackValue;
      this.addLogMessage("monster", "attack", `-${attackValue}`);
    },
    specialAttackMonster: function () {
      this.currentRound++;
      const attackValue = getRandomValue(10, 25);
      this.monsterHealth -= attackValue;
      this.addLogMessage("player", "attack", `-${attackValue}`);
      this.attackPlayer();
    },
    healPlayer: function () {
      this.currentRound++;
      const healValue = getRandomValue(8, 20);
      this.playerHealth += healValue;
      this.addLogMessage("player", "heal", `+${healValue}`);
      this.attackPlayer();
    },
    surrender: function () {
      this.winner = "monster";
    },
    addLogMessage: function (who, what, value) {
      this.logMessages.unshift({
        actionBy: who,
        actionType: what,
        actionValue: value,
      });
    },
  },

  computed: {
    monsterBarStyles: function () {
      if (this.monsterHealth < 0) {
        return { width: "0%" };
      }
      return { width: this.monsterHealth + "%" };
    },
    playerBarStyles: function () {
      if (this.playerHealth < 0) {
        return { width: "0%" };
      }
      return { width: this.playerHealth + "%" };
    },
    specialAttackToggle: function () {
      return this.currentRound % 3 !== 0;
    },
    healToggle: function () {
      return this.playerHealth === 100;
    },
  },
  watch: {
    playerHealth: function (value) {
      if (value <= 0 && this.monsterHealth <= 0) this.winner = "draw";
      else if (value <= 0) this.winner = "monster";
    },
    monsterHealth: function (value) {
      if (value <= 0 && this.playerHealth <= 0) this.winner = "draw";
      else if (value <= 0) this.winner = "player";
    },
  },
});

app.mount("#game");
