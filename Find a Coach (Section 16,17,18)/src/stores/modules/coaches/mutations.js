export default {
  registerCoach: function(state, payload) {
    state.coaches.push(payload);
  },
  setCoaches: function(state, payload) {
    state.coaches = payload;
  },
  setFetchTimeStamp: function(state) {
    state.lastFetch = new Date().getTime();
  }
};
