export default {
  registerCoach: async function(context, data) {
    const userId = context.rootGetters.userId;
    const coachData = {
      firstName: data.first,
      lastName: data.last,
      areas: data.areas,
      description: data.desc,
      hourlyRate: data.rate
    };
    const token = context.rootGetters.token;
    const response = await fetch(
      `https://vue-http-demo-d9262-default-rtdb.firebaseio.com/coaches/${userId}.json?auth=${token}`,
      {
        method: 'PUT',
        body: JSON.stringify(coachData)
      }
    );
    // const responseData = await response.json();
    response;
    context.commit('registerCoach', {
      ...coachData,
      id: userId
    });
  },
  loadCoaches: async function(context, payload) {
    // Should we update coaches
    if (!payload.forceRefresh && !context.getters.shouldUpdate) return;

    const response = await fetch(
      `https://vue-http-demo-d9262-default-rtdb.firebaseio.com/coaches.json`
    );
    const responseData = await response.json();
    if (!response.ok)
      throw new Error(responseData.message || 'Failed To Fetch.');

    const coaches = [];
    for (const key in responseData) {
      const coach = {
        id: key,
        firstName: responseData[key].firstName,
        lastName: responseData[key].lastName,
        areas: responseData[key].areas,
        description: responseData[key].description,
        hourlyRate: responseData[key].hourlyRate
      };
      coaches.push(coach);
    }
    context.commit('setCoaches', coaches);
    context.commit('setFetchTimeStamp');
  }
};
