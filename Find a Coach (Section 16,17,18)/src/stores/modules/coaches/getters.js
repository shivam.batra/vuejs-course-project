export default {
  coaches: function(state) {
    return state.coaches;
  },
  hasCoaches: function(state) {
    return state.coaches && state.coaches.length > 0;
  },
  isCoach: function(_, getters, _2, rootGetters) {
    const coaches = getters.coaches;
    const userId = rootGetters.userId;

    return coaches.some(coach => coach.id === userId);
  },
  shouldUpdate: function(state) {
    const lastFetch = state.lastFetch;
    if (!lastFetch) return true;
    const currentTimeStamp = new Date().getTime();
    return (currentTimeStamp - lastFetch) / 1000 > 60;
  }
};
