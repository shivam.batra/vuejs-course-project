export default {
  addRequest: function(state, payload) {
    state.requests.push(payload);
  },
  setRequests: function(state, payload) {
    state.requests = payload;
  }
};
