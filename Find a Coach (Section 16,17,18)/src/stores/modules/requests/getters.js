export default {
  requests: function(state, _, _2, rootGetters) {
    const coachId = rootGetters.userId;
    return state.requests.filter(req => req.coachId === coachId);
  },
  hasRequests: function(_, getters) {
    return getters.requests && getters.requests.length > 0;
  }
};
