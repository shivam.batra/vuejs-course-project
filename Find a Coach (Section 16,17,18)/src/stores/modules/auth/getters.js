export default {
  userId: function(state) {
    return state.userId;
  },
  token: function(state) {
    return state.token;
  },
  isAuthenticated: function(state) {
    return !!state.token;
  },
  didAutoLogout: function(state) {
    return state.didAutoLogout;
  }
};
