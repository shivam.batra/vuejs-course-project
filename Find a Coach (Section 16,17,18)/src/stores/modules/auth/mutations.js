export default {
  setUser: function(state, payload) {
    state.token = payload.token;
    state.userId = payload.userId;
  },
  setAutoLogout: function(state) {
    state.didAutoLogout = true;
  }
};
